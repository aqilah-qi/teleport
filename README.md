# Teleport Assessment

## Hello!

The name's Qi. Here's my assessment. Hope this meets the reqirements.

[https://qi-teleport.netlify.app](https://qi-teleport.netlify.app)

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```
